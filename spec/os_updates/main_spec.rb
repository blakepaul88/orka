# frozen_string_literal: true

require 'spec_helper'

context 'roles/os_updates' do
  # check that the OS is up to date
  describe 'softwareupdate' do
    it 'is up to date' do
        # inspired by https://github.com/bp88/JSS-Scripts/blob/5e4fcab8dd85fcbec260d07a2a855538dac0b8d3/AppleSoftwareUpdate.sh#L365
        softwareupdate_out = "/tmp/softwareupdate_out"
        softwareupdate_cmd = command("softwareupdate -l 2>&1 > #{softwareupdate_out}")
        expect(softwareupdate_cmd).to be_a_successful_cmd

        # grep exits with a code of 1 when there are no matches
        has_updates_cmd = command("grep -i recommended #{softwareupdate_out}")
        expect(has_updates_cmd).not_to be_a_successful_cmd
    end
  end
end
