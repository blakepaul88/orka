# frozen_string_literal: true

require 'serverspec'
require 'pry'
require 'English'

set :backend, :ssh
set :disable_sudo, true
set :os, { family: 'darwin' }
set :shell, '/bin/zsh'

HOST = ENV.fetch('TARGET_HOST')
PORT = ENV.fetch('TARGET_PORT')

options = {
  user: 'gitlab',
  port: PORT,
  user_known_hosts_file: '/dev/null',
  verify_host_key: :never
}

options[:password] = ENV['LOGIN_PASSWORD'] if ENV.key?('LOGIN_PASSWORD')

set :host, HOST
set :ssh_options, options

def run_on_target(cmd)
  ssh = ENV.key?('LOGIN_PASSWORD') ? "sshpass -p #{ENV['LOGIN_PASSWORD']} ssh" : 'ssh'
  out = `#{ssh} -p #{PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@#{HOST} #{cmd}`
  raise "unable to run `#{cmd}` on target: #{out}" if $CHILD_STATUS.exitstatus != 0

  out
end

def with_fixtures
  rsync = ENV.key?('LOGIN_PASSWORD') ? "sshpass -p #{ENV['LOGIN_PASSWORD']} rsync" : 'rsync'
  with_tmpdir do |tmpdir|
    out = `#{rsync} -arvz -e 'ssh -p #{PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null' --delete spec/fixtures/ gitlab@#{HOST}:#{tmpdir}`
    raise "unable to setup fixtures on target: #{out}" if $CHILD_STATUS.exitstatus != 0

    yield tmpdir
  end
end

def with_tmpdir
  tmpdir = command('mktemp -d').stdout.strip
  yield tmpdir
ensure
  command("rm -rf #{tmpdir}")
end

RSpec::Matchers.define :be_a_successful_cmd do |_expected|
  match do |actual|
    actual.exit_status == 0
  end
  failure_message do |actual|
    "expected that `#{actual.name}` would be successful, got the following output:\n#{actual.stdout}\n#{actual.stderr}"
  end
end

def next?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'next'
end

def big_sur?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'big-sur'
end

def catalina?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'catalina'
end

def mojave?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'mojave'
end

def high_sierra?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'high-sierra'
end
