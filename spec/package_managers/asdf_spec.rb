# frozen_string_literal: true

require 'spec_helper'

context 'roles/package_managers' do
  context 'asdf' do
    # Install dir for asdf should exist
    describe file('/Users/gitlab/.asdf') do
      it { should be_directory }
    end

    # asdf includes a quote from Rajinikanth in their help output. Pretty useful to check it works!
    describe command('asdf help') do
      it { should be_a_successful_cmd } unless high_sierra? || mojave? || catalina? # asdf help exits 1 before 0.8.0 ...
      its(:stdout) { should match('Rajinikanth') }
    end

    # PATH should contain shims & bin
    describe command('printenv') do
      it { should be_a_successful_cmd }
      its(:stdout) { should match(%r{^PATH=(.*:)?/Users/gitlab/.asdf/shims(:.*)?$}) }
      its(:stdout) { should match(%r{^PATH=(.*:)?/Users/gitlab/.asdf/bin(:.*)?$}) }
    end
  end
end
