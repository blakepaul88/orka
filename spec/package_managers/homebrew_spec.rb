# frozen_string_literal: true

require 'spec_helper'

context 'roles/package_managers' do
  context 'homebrew' do
    # Install dir for homebrew should exist
    describe file('/usr/local/Homebrew/.git') do
      it { should be_directory }
    end

    # Check that this actually runs homebrew
    describe command('brew help') do
      it { should be_a_successful_cmd }
      its(:stdout) { should match(/brew install FORMULA/) }
    end

    # Homebrew should not auto update
    if high_sierra?
      describe command('printenv') do
        it { should be_a_successful_cmd }
        its(:stdout) { should match(/^HOMEBREW_NO_AUTO_UPDATE=1$/) }
      end
    end

    # Taps should be tapped
    context 'taps' do
      describe 'homebrew/homebrew-core' do
        let(:subject) { file('/usr/local/Homebrew/Library/Taps/homebrew/homebrew-core') }
        it { should be_directory }
      end

      describe 'homebrew/homebrew-cask' do
        let(:subject) { file('/usr/local/Homebrew/Library/Taps/homebrew/homebrew-cask') }
        it { should be_directory }
      end

      describe 'gitlab/homebrew-shared-runners' do
        let(:subject) { file('/usr/local/Homebrew/Library/Taps/gitlab/homebrew-shared-runners') }
        it { should be_directory }
      end
    end

    # Built-in tests should pass for each installed formula
    context 'built-in formula tests' do
      brew_list = high_sierra? || mojave? || catalina? ? 'brew list -1' : 'brew list --formula -1'
      run_on_target(brew_list).strip.each_line do |formula|
        formula = formula.strip
        next if formula == 'libmetalink' # this formula doesn't have tests, no need to list it in the skipped list
        next if formula == 'liblqr' # this formula doesn't have tests, no need to list it in the skipped list
        next if formula == 'git' && (high_sierra? || mojave? || catalina?) # git built-in test is broken before catalina, we do our own test below
        next if formula == 'six' # this is a dependency of awscli and it has test dependencies which we don't install

        opts = {}
        opts[:skip] = "error: implicit declaration of function 'u32_uctomb' is invalid in C99" if formula == 'libunistring' && catalina?

        describe formula do
          it 'should pass', opts do
            cmd = command("brew test #{formula}")
            expect(cmd).to be_a_successful_cmd
          end
        end
      end
    end
  end
end
