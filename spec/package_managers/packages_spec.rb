# frozen_string_literal: true

require 'spec_helper'

context 'roles/package_managers' do
  context 'installed packages' do
    brew_list = high_sierra? || mojave? || catalina? ? 'brew list -1' : 'brew list --formula -1'
    brew_cask_list = high_sierra? || mojave? || catalina? ? 'brew cask list -1' : 'brew list --casks -1'

    describe 'dotnet' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current dotnet-core")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can build an an app' do
        with_fixtures do |fixture_dir|
          target = nil
          target = 'netcoreapp3.1' if big_sur?
          target = 'netcoreapp3.0' if catalina?
          target = 'netcoreapp2.1' if mojave?
          target = 'netcoreapp2.0' if high_sierra?
          if target
            cmd = command("cd #{fixture_dir}/dotnet-core && sed -ibak 's/netcoreapp5.0/#{target}/' myApp.csproj")
            expect(cmd).to be_a_successful_cmd
          end

          cmd = command("cd #{fixture_dir}/dotnet-core && dotnet run")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello World!')
        end
      end
    end

    describe 'bazel' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can build an a binary' do
        with_fixtures do |fixture_dir|
          cmd = command("cd #{fixture_dir}/bazel && bazel build //main:hello-world")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stderr).to match('bazel-bin/main/hello-world')

          cmd = command("cd #{fixture_dir}/bazel && bazel-bin/main/hello-world")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello world')
        end
      end
    end

    unless high_sierra? || mojave?
      describe 'flutter' do
        it 'is installed and globally active' do
          if high_sierra? || mojave? || catalina? || big_sur?
            cmd = command("asdf current #{subject}")
            expect(cmd).to be_a_successful_cmd
            expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
          else
            cmd = command("#{brew_cask_list} #{subject}")
            expect(cmd).to be_a_successful_cmd
          end
        end

        it 'can create an app' do
          cmd = command('flutter create my_app')
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('All done!')
        end
      end
    end

    describe 'rust' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can compile hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("cd #{fixture_dir}/rust && rustc hello.rs")
          expect(cmd).to be_a_successful_cmd

          cmd = command("cd #{fixture_dir}/rust && ./hello")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello World!')
        end
      end
    end

    describe 'golang' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can compile hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("go run #{fixture_dir}/go/main.go")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello, world!')
        end
      end
    end

    describe 'gradle' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      opts = {}
      opts[:skip] = 'unable to compile' if high_sierra? || mojave? || catalina?
      it 'can compile an app', opts do
        with_fixtures do |fixture_dir|
          cmd = command("cd #{fixture_dir}/gradle && gradle compileJava")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('BUILD SUCCESSFUL')

          file = file("#{fixture_dir}/gradle/build/classes/java/main/demo/App.class")
          expect(file).to be_file
        end
      end
    end

    describe 'maven' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      opts = {}
      opts[:skip] = 'something is overriding JAVA_HOME' if high_sierra? || mojave?
      it 'can compile an app', opts do
        with_fixtures do |fixture_dir|
          cmd = command("cd #{fixture_dir}/maven && mvn compile")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('BUILD SUCCESS')

          file = file("#{fixture_dir}/maven/target/classes/com/mycompany/app/App.class")
          expect(file).to be_file
        end
      end
    end

    describe 'nodejs' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can execute hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("node #{fixture_dir}/node/hello.js")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('hello, world!')
        end
      end
    end

    describe 'ruby' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can execute hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("ruby #{fixture_dir}/ruby/hello.rb")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('hello, world!')
        end
      end
    end

    describe 'java' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      # JAVA_HOME should be set
      it 'should set $JAVA_HOME' do
        cmd = command('printenv')
        expect(cmd).to be_a_successful_cmd
        if high_sierra? || mojave? || catalina? || big_sur?
          expect(cmd.stdout).to match(%r{^JAVA_HOME=/Users/gitlab/.asdf/installs/java/adoptopenjdk-.*$})
        else
          expect(cmd.stdout).to match(%r{^JAVA_HOME=/usr/local/Cellar/openjdk/.*$})
        end
      end

      it 'can execute hello world' do
        with_fixtures do |fixture_dir|
          cmd = high_sierra? ? command("cd #{fixture_dir}/java && javac hello.java && java HelloWorld") : command("java #{fixture_dir}/java/hello.java")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello, World!')
        end
      end
    end

    describe 'python' do
      it 'is installed and globally active' do
        if high_sierra? || mojave?
          cmd = command("asdf list #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/^(\s+)?2\./)
          expect(cmd.stdout).to match(/^(\s+)?3\./)
        elsif catalina? || big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can execute hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("python2 #{fixture_dir}/python/hello2.py")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('hello from python 2')

          cmd = command("python3 #{fixture_dir}/python/hello3.py")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('hello from python 3')

          default = high_sierra? || mojave? ? '2' : '3'

          cmd = command("python #{fixture_dir}/python/hello#{default}.py")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match("hello from python #{default}")
        end
      end
    end

    describe 'google-cloud-sdk' do
      it 'is installed and globally active' do
        if high_sierra? || mojave? || catalina? || big_sur?
          cmd = command("asdf current gcloud")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_cask_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can install components' do
        cmd = command('gcloud components install bigtable --quiet')
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stderr).to match('Installing: Cloud Bigtable Emulator')
      end
    end

    describe 'postgres' do
      it 'is installed and globally active' do
        if big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      if big_sur?
        it 'can execute SQL queries' do
          cmd = command('pg_ctl start -l /tmp/start.log || (cat /tmp/start.log; exit 1)')
          expect(cmd).to be_a_successful_cmd
          cmd = command("psql --dbname postgres --command 'SELECT 1;'")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/\(1 row\)/)
          cmd = command('pg_ctl stop')
          expect(cmd).to be_a_successful_cmd
        end
      else
        it 'can execute SQL queries' do
          cmd = command('launchctl load /usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist && sleep 5')
          expect(cmd).to be_a_successful_cmd
          cmd = command("psql --dbname postgres --command 'SELECT 1;'")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/\(1 row\)/)
          cmd = command('launchctl unload /usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist')
          expect(cmd).to be_a_successful_cmd
        end
      end
    end

    describe 'php' do
      it 'is installed and globally active' do
        if big_sur?
          cmd = command("asdf current #{subject}")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
        else
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end
      end

      it 'can execute hello world' do
        with_fixtures do |fixture_dir|
          cmd = command("php #{fixture_dir}/php/hello.php")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello World')
        end
      end
    end

    describe 'git' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'should allow you to commit' do
        with_tmpdir do |tmpdir|
          cmd = command("cd #{tmpdir} && touch file")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git init .")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git add file")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git config user.email 'you@example.com'")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git config user.name 'Your Name'")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git commit -am test")
          expect(cmd).to be_a_successful_cmd
        end
      end
    end

    describe 'coreutils' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'works' do
        cmd = command('gls /Users/gitlab')
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Documents')
      end
    end

    gcc = big_sur? ? 'gcc@9' : 'gcc'
    describe gcc do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      # Skipping for now due to:
      #
      # In file included from /var/folders/lr/5gw1xrv53h1b_9mjjvs2k1dr0000gn/T/tmp.ABdxpz3h/gcc/hello.c:1:
      # /usr/local/Cellar/gcc@9/9.3.0/lib/gcc/9/gcc/x86_64-apple-darwin20/9.3.0/include-fixed/stdio.h:78:10: fatal error: _stdio.h: No such file or directory
      #    78 | #include <_stdio.h>
      #       |          ^~~~~~~~~~
      # compilation terminated.
      opts = {}
      opts[:skip] = 'unable to find _stdio.h from Command-Line Tools' if big_sur?
      it 'compiles a C file', opts do
        binary = 'gcc-11'
        binary = 'gcc-9' if catalina? || big_sur?
        binary = 'gcc-8' if mojave? || high_sierra?

        with_fixtures do |fixture_dir|
          cmd = command("#{binary} #{fixture_dir}/gcc/hello.c -o hello")
          expect(cmd).to be_a_successful_cmd

          cmd = command('./hello')
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hello, World!')
        end
      end
    end

    describe 'carthage' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'should install a Cartfile' do
        with_fixtures do |fixture_dir|
          cmd = command("cd #{fixture_dir}/carthage && carthage update --no-build")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Checking out AFNetworking/)

          file = file("#{fixture_dir}/carthage/Cartfile.resolved")
          expect(file).to be_file
          expect(file.content).to match('AFNetworking')
        end
      end
    end

    describe 'curl' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'should get google' do
        cmd = command('curl https://www.google.com/generate_204')
        expect(cmd).to be_a_successful_cmd
      end
    end

    describe 'git-lfs' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'should track files' do
        with_tmpdir do |tmpdir|
          cmd = command("cd #{tmpdir} && dd if=/dev/zero of=file.iso count=1024 bs=1024")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git init .")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git lfs install")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git lfs track '*.iso'")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git add file.iso")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git config --global user.email 'you@example.com'")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git config --global user.name 'Your Name'")
          expect(cmd).to be_a_successful_cmd
          cmd = command("cd #{tmpdir} && git commit -am test")
          expect(cmd).to be_a_successful_cmd

          cmd = command("cd #{tmpdir} && git lfs ls-files")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('file.iso')
        end
      end
    end

    describe 'wget' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'should get google' do
        cmd = command('wget https://www.google.com/generate_204')
        expect(cmd).to be_a_successful_cmd
      end
    end

    describe 'openssl@1.1' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'is able to show a certificate' do
        with_fixtures do |fixture_dir|
          cmd = command("/usr/local/opt/openssl@1.1/bin/openssl x509 -in #{fixture_dir}/openssl/certificate.crt -text")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('CN = serverspec')
        end
      end
    end

    describe 'jq' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'can parse json' do
        with_fixtures do |fixture_dir|
          cmd = command("jq -r '.foo' #{fixture_dir}/jq/test.json")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('bar')
        end
      end
    end

    describe 'gpg' do
      it 'is installed' do
        cmd = command("#{brew_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'can import key' do
        with_fixtures do |fixture_dir|
          cmd = command("gpg --import #{fixture_dir}/gpg/public.key")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stderr).to match('Total number processed: 1')
        end
      end
    end

    unless high_sierra?
      describe 'sshpass' do
        it 'is installed' do
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end

        it 'can provide password to connect to localhost' do
          password = ENV.fetch('LOGIN_PASSWORD', 'gitlab')
          cmd = command("sshpass -p #{password} ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@localhost uname")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Darwin')
        end
      end
    end

    describe 'powershell' do
      it 'is installed' do
        cmd = command("#{brew_cask_list} #{subject}")
        expect(cmd).to be_a_successful_cmd
      end

      it 'can execute commands' do
        cmd = command("pwsh -Command '$PSVersionTable'")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('PSVersion')
      end
    end

    unless high_sierra? || mojave? || catalina? || big_sur?
      describe 'htop' do
        it 'is installed' do
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end

        it 'can run help command' do
          cmd = command("htop -h")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match('Hisham Muhammad')
        end
      end
    end

    unless high_sierra? || mojave? || catalina? || big_sur?
      describe 'awscli' do
        it 'is installed' do
          cmd = command("#{brew_list} #{subject}")
          expect(cmd).to be_a_successful_cmd
        end

        it 'can download a public file' do
          with_tmpdir do |tmpdir|
            cmd = command("aws --region us-east-1 s3 cp s3://gitlab-runner-custom-autoscaler-downloads/v0.5.0/autoscaler-linux-amd64 #{tmpdir} --no-sign-request")
            expect(cmd).to be_a_successful_cmd
            expect(file("#{tmpdir}/autoscaler-linux-amd64")).to be_file
          end
        end
      end
    end
  end
end
